export function isUrlInternal(url) {
  if (typeof url !== "string" || !url || url === "") return;

  if (url.startsWith("/")) return true;
  let cleanUrl = url.replace("http://", "").replace("https://");
  cleanUrl = cleanUrl.split("/")[0];

  let localUrl = window.location.href
    .replace("http://", "")
    .replace("https://");
  localUrl = localUrl.split("/")[0];

  if (
    [cleanUrl, cleanUrl.replace("www.")].sort().join(",") ===
    [localUrl, localUrl.replace("www.")].sort().join(",")
  )
    return true;

  return false;
}
