export class ValidationRule {
  static required(msg) {
    return {
      type: "required",
      params: [msg],
    };
  }
  static email(msg) {
    return {
      type: "email",
      params: [msg],
    };
  }
  static numeric(msg) {
    return {
      type: "matches",
      params: [/^\d+$/, msg],
    };
  }
  static regex(regex, msg) {
    return {
      type: "matches",
      params: [regex, msg],
    };
  }
  static min(length, msg) {
    return {
      type: "min",
      params: [length, msg],
    };
  }
  static max(length, msg) {
    return {
      type: "max",
      params: [length, msg],
    };
  }
  static isEqualToField(field, msg) {
    return {
      type: "test",
      params: [
        "passwords-match",
        msg,
        (a, b) => {
          return a === b.parent[field];
        },
      ],
    };
  }
  static mustBe(value, msg) {
    return {
      type: "test",
      params: [
        "must-be",
        msg,
        (a, b) => {
          return a === value;
        },
      ],
    };
  }
  static minDateAfterNow(msg) {
    return {
      type: "test",
      params: [
        "min-date-after-now",
        msg,
        (a) => {
          return new Date(a) >= new Date();
        },
      ],
    };
  }
}
