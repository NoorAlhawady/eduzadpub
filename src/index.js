import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import { SharedAPIDataContextProvider } from 'contexts';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <SharedAPIDataContextProvider>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </SharedAPIDataContextProvider>
  </React.StrictMode>
);