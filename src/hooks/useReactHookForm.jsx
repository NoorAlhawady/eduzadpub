// import * as yup from "yup";
// import { useForm } from "react-hook-form";
// import { yupResolver } from "@hookform/resolvers/yup";
// import { useCallback, useState, useMemo, useEffect } from "react";

// export function useReactHookForm({ defaultValues, schema, mode }) {
//   const [currentSchema, setCurrentSchema] = useState(schema ?? null);
//   const {
//     control,
//     setValue,
//     handleSubmit,
//     reset,
//     watch,
//     getValues,
//     formState,
//     setError,
//     register,
//   } = useForm({
//     defaultValues: defaultValues ? defaultValues : undefined,
//     resolver: currentSchema
//       ? yupResolver(createYupSchema(currentSchema))
//       : undefined,
//     mode,
//   });

//   const updateSchema = useCallback((newSchema, replace = false) => {
//     if (replace) {
//       return setCurrentSchema(newSchema);
//     }
//     setCurrentSchema((prevSchema) => [
//       ...(prevSchema ? prevSchema : []),
//       ...newSchema,
//     ]);
//   }, []);

//   const updateValues = useCallback(
//     (newValues) => {
//       // Object.keys(newValues).forEach((key) => {
//       //   setValue(key, newValues[key]);
//       // });
//       reset({
//         ...Object.keys(newValues).reduce((acc, key) => {
//           acc[key] = newValues[key];
//           return acc;
//         }, {}),
//       });
//     },
//     [reset]
//   );

//   // ** Side Effects
//   useEffect(() => {
//     if (defaultValues) {
//       updateValues(defaultValues);
//     }
//     // eslint-disable-next-line react-hooks/exhaustive-deps
//   }, [JSON.stringify(defaultValues), updateValues]);

//   return useMemo(
//     () => ({
//       updateSchema,
//       control,
//       setValue,
//       updateValues,
//       handleSubmit,
//       reset,
//       watch,
//       getValues,
//       setError,
//       register,
//       formState,
//     }),
//     [
//       updateSchema,
//       control,
//       setValue,
//       updateValues,
//       handleSubmit,
//       reset,
//       watch,
//       getValues,
//       setError,
//       register,
//       formState,
//     ]
//   );
// }

// function createYupSchema(fields) {
//   const schema = fields.reduce((schema, field) => {
//     const { name, type, typeError, validations = [] } = field;
//     const isObject = name.indexOf(".") >= 0;

//     if (!yup[type]) {
//       return schema;
//     }
//     let validator = yup[type]().typeError(typeError || "");
//     validations.forEach((validation) => {
//       const { params, type } = validation;
//       if (!validator[type]) {
//         return;
//       }
//       validator = validator[type](...params);
//     });

//     if (!isObject) {
//       return schema.concat(yup.object().shape({ [name]: validator }));
//     }

//     const reversePath = name.split(".").reverse();
//     const currNestedObject = reversePath.slice(1).reduce(
//       (yupObj, path, index, source) => {
//         if (!isNaN(path)) {
//           return { array: yup.array().of(yup.object().shape(yupObj)) };
//         }
//         if (yupObj.array) {
//           return { [path]: yupObj.array };
//         }
//         return { [path]: yup.object().shape(yupObj).required().nullable() };
//       },
//       { [reversePath[0]]: validator }
//     );

//     const newSchema = yup.object().shape(currNestedObject);
//     return schema.concat(newSchema);
//   }, yup.object().shape({}));

//   return schema;
// }
