import { useCallback } from "react";
import { useApiService } from "./useApiService";

export function useApiPost({ path, options = {} }) {
  const { resultParser, initialLoadingState = false } = options;

  // ** Hooks
  const { result, error, loading, statusCode, fetchData, cancel } =
    useApiService(path, "POST", initialLoadingState, resultParser);

  // ** Functions
  const post = useCallback(
    async (args) => {
      fetchData(args);
    },
    [fetchData]
  );

  return { result, error, loading, statusCode, post, cancel };
}
